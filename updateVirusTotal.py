import random
from os import listdir
from os.path import isfile, join

PATH = [
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Andromeda-Botnet-CTU-Malware-Capture-Botnet-168-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Andromeda-Botnet-CTU-Malware-Capture-Botnet-168-2',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Colbalt-CTU-Malware-Capture-Botnet-345-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Cridex-CTU-Malware-Capture-Botnet-108-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Cridex-CTU-Malware-Capture-Botnet-109-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Dridex-CTU-Malware-Capture-Botnet-153-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Dridex-CTU-Malware-Capture-Botnet-218-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Dridex-CTU-Malware-Capture-Botnet-227-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Dridex-CTU-Malware-Capture-Botnet-228-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Dridex-CTU-Malware-Capture-Botnet-246-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Dridex-CTU-Malware-Capture-Botnet-248-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Dridex-CTU-Malware-Capture-Botnet-249-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Dridex-CTU-Malware-Capture-Botnet-257-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Dridex-CTU-Malware-Capture-Botnet-259-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Dridex-CTU-Malware-Capture-Botnet-260-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Dridex-CTU-Malware-Capture-Botnet-262-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Dridex-CTU-Malware-Capture-Botnet-322-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Dridex-CTU-Malware-Capture-Botnet-326-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Dridex-CTU-Malware-Capture-Botnet-346-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Emotet-CTU-Malware-Capture-Botnet-114-3',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Emotet-CTU-Malware-Capture-Botnet-124-1-bak',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Emotet-CTU-Malware-Capture-Botnet-168-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Emotet-CTU-Malware-Capture-Botnet-264-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Emotet-CTU-Malware-Capture-Botnet-264-2',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Emotet-CTU-Malware-Capture-Botnet-269-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Emotet-CTU-Malware-Capture-Botnet-271-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Emotet-CTU-Malware-Capture-Botnet-272-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Emotet-CTU-Malware-Capture-Botnet-276-1',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Emotet-CTU-Malware-Capture-Botnet-276-2',
	'D:\\Learning\\APT\\apt-dataset\\Flow\\Emotet-CTU-Malware-Capture-Botnet-279-1'	
]


TITTLE = 'Src IP,Dst IP,Tot Fwd Pkts,Tot Bwd Pkts,TotLen Fwd Pkts,TotLen Bwd Pkts,Fwd Pkt Len Max,Fwd Pkt Len Min,Fwd Pkt Len Mean,Fwd Pkt Len Std,Bwd Pkt Len Max,Bwd Pkt Len Min,Bwd Pkt Len Mean,Bwd Pkt Len Std,Flow Byts/s,Flow Pkts/s,Flow IAT Mean,Flow IAT Std,Flow IAT Max,Flow IAT Min,Fwd IAT Tot,Fwd IAT Mean,Fwd IAT Std,Fwd IAT Max,Fwd IAT Min,Bwd IAT Tot,Bwd IAT Mean,Bwd IAT Std,Bwd IAT Max,Bwd IAT Min,Fwd PSH Flags,Bwd PSH Flags,Fwd URG Flags,Bwd URG Flags,Fwd Header Len,Bwd Header Len,Fwd Pkts/s,Bwd Pkts/s,Pkt Len Min,Pkt Len Max,Pkt Len Mean,Pkt Len Std,Pkt Len Var,FIN Flag Cnt,SYN Flag Cnt,RST Flag Cnt,PSH Flag Cnt,ACK Flag Cnt,URG Flag Cnt,CWE Flag Count,ECE Flag Cnt,Down/Up Ratio,Pkt Size Avg,Fwd Seg Size Avg,Bwd Seg Size Avg,Fwd Byts/b Avg,Fwd Pkts/b Avg,Fwd Blk Rate Avg,Bwd Byts/b Avg,Bwd Pkts/b Avg,Bwd Blk Rate Avg,Subflow Fwd Pkts,Subflow Fwd Byts,Subflow Bwd Pkts,Subflow Bwd Byts,Init Fwd Win Byts,Init Bwd Win Byts,Fwd Act Data Pkts,Fwd Seg Size Min,Active Mean,Active Std,Active Max,Active Min,Idle Mean,Idle Std,Idle Max,Idle Min,Label\n'

def getFileName(path):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    return onlyfiles
	

#read file by path return lines value
def readFile(path):
	fila = open(path, "r")
	lines = fila.readlines()
	return lines

#write file by path and values (type list)
def writeFile(path, values):
	fil = open(path, "w")
	for value in values:
		count = 0
		for v in value:
			count = count + 1
			if count == len(value):
				fil.write(str(v))
			else:
				fil.write(str(v) + ',')
	fil.close()

def main(PATH_, ips_apt):
	f_files = getFileName(PATH_)
	print(f_files[0])
	features = readFile(PATH_ + "//" + f_files[0])
	del features[0]
	new_file = list()
	TITTLE_ = TITTLE.split(",")
	new_file.append(TITTLE_)
	for feature_line in features:
		feature_line = feature_line.split(",")
		ip_src = feature_line[1]
		ip_dst = feature_line[3]
		
		del feature_line[0:8]
			
		feature_line = [ip_dst] + feature_line
		feature_line = [ip_src] + feature_line
		
		if ip_src in ips_apt or ip_dst in ips_apt:
			feature_line[len(feature_line) - 1] = "1\n"
		else:
			feature_line[len(feature_line) - 1] = "0\n"
		#for ip_apt in ips_apt:		
		#	if ip_apt == ip_src or ip_apt == ip_dst:
		#		
		#		feature_line[len(feature_line) - 1] = "1\n"
		#		#feature_line = str(feature_line[len(features) - 1])
		#		#print(feature_line)
		#		#new_file.append(feature_line)
		#	else:
		#		feature_line[len(feature_line) - 1] = "0\n"
		new_file.append(feature_line)
	name = f_files[0].split(".")
	name = name[0] + "." + name[1] + ".splitVirustotal." + name[2]
	writeFile("D:\\Learning\\APT\\apt-dataset\\Flow\\Tong_hop_virustotal\\" + name, new_file)
	

ips_apt = list()
ips_apt_file = readFile('D:\\Learning\\APT\\apt-dataset\\Flow\\getPublicIP\\apt_ip_virustotal.csv')
for ip in ips_apt_file:
	ip = ip.split(",")
	ip = ip[0]
	ips_apt.append(ip)
#print(ips_apt[0])	
for pa in PATH:
	#print(pa)
	main(pa, ips_apt)
	#break