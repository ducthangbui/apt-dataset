'''
Tong hop file tu cac file da split

'''

import random
from os import listdir
from os.path import isfile, join

PATH = 'D:\\Learning\\APT\\Data\\Flow\\Tong_hop'
PATH_Total = 'D:\\Learning\\APT\\Data\\Flow\\All'

def getFileName(path):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    return onlyfiles
	

#read file by path return lines value
def readFile(path):
	fila = open(path, "r")
	lines = fila.readlines()
	return lines

#write file by path and values (type list)
def writeFile(path, values):
    #print(values)
    fil = open(path, "a")
    count = 0
    for value in values:
        count = count + 1
        fil.write(value + ",")
        if count == len(values):
            fil.write("\n")
    fil.close()

def writeAPTMal(path, pathTrain):
    fileNames = getFileName(path)
    for fileName in fileNames:
        print(fileName)
        lines = readFile(path + "\\" + fileName)
        for line in lines:
            line = line.split(",")
            #print(line)
            
            if "Mal-" in line[len(line) - 1]:
                line[len(line) - 1] = "1"
                writeFile(pathTrain + "\\Malv2.csv", line)
            else:
                line[len(line) - 1] = "0"
                writeFile(pathTrain + "\\NoMalv2.csv", line)
        #break
writeAPTMal(PATH, PATH_Total)