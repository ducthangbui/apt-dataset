import os
os.environ["PYSPARK_PYTHON"] = "/usr/bin/python"
import glob
import requests
import ipaddress
inputPath = "dataset"
outputPath = "fixed_dataset"
url = "https://api.threatminer.org/v2/host.php?rt=4&q="
url2 = "https://feodotracker.abuse.ch/host/"
publicIPs = {}
with open("wrongIps.csv","r") as f:
    for line in f:
        data = line.split(",")
        if (not ipaddress.ip_address(data[0]).is_private and not data[0] in publicIPs):
            publicIPs[data[0]] = data[2]
        if (not ipaddress.ip_address(data[1]).is_private and not data[1] in publicIPs):
            publicIPs[data[1]] = data[2]
print("Total publicIPs: ", len(publicIPs))
count = 0
for ip in publicIPs:
    print("IP: ", ip)
    if ip == "239.255.255.250" or ip == "224.0.0.1" or ip == "8.8.4.4" or ip == "8.8.8.8":
        print("fix ", ip, "-> BENIGN")
        publicIPs[ip] = "0"
        count += 1
    else:
        response = requests.get(url+ip)
        result = response.json()["results"]
        if len(result) > 0 and publicIPs[ip] == "BENIGN":
            print("fix ", ip, "-> SUSPICIOUS")
            publicIPs[ip] = "1"
            count += 1
        else:
            print("request2")
            response = requests.get(url2+ip)
            result = response.text.find("<table")
            if result != -1 and publicIPs[ip] == "BENIGN":
                print("fix ", ip, "-> SUSPICIOUS")
                publicIPs[ip] = "1"
                count += 1
            elif publicIPs[ip] == "BENIGN":
                publicIPs[ip] = "0"
            elif publicIPs[ip] == "SUSPICIOUS":
                publicIPs[ip] = "1"
print(publicIPs)
print("Total fixed ips: ", count, "/", len(publicIPs))
for filename in glob.glob(os.path.join(inputPath, "*.csv")):
    with open(filename, "r") as f1:
        print("Opening: ", filename)
        with open(os.path.join(outputPath, filename.split("/")[1]), "w") as f2:
            print("Writing: ", os.path.join(outputPath, filename.split("/")[1]))
            for line in f1:
                data = line.split(",")
                if data[-1] == "Label":
                    f2.write(line)
                else:
                    if data[0] in publicIPs:
                        data[-1] = publicIPs[data[0]] + "\n"
                        line = ",".join(data)
                    elif data[1] in publicIPs:
                        data[-1] = publicIPs[data[1]] + "\n"
                        line = ",".join(data)
                    f2.write(line)
            
