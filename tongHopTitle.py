
'''
Tong hop file tu cac file da split co them Title

'''

import random
from os import listdir
from os.path import isfile, join

TITLE = "Tot Fwd Pkts,Tot Bwd Pkts,TotLen Fwd Pkts,TotLen Bwd Pkts,Fwd Pkt Len Max,Fwd Pkt Len Min,Fwd Pkt Len Mean,Fwd Pkt Len Std,Bwd Pkt Len Max,Bwd Pkt Len Min,Bwd Pkt Len Mean,Bwd Pkt Len Std,Flow Byts/s,Flow Pkts/s,Flow IAT Mean,Flow IAT Std,Flow IAT Max,Flow IAT Min,Fwd IAT Tot,Fwd IAT Mean,Fwd IAT Std,Fwd IAT Max,Fwd IAT Min,Bwd IAT Tot,Bwd IAT Mean,Bwd IAT Std,Bwd IAT Max,Bwd IAT Min,Fwd PSH Flags,Bwd PSH Flags,Fwd URG Flags,Bwd URG Flags,Fwd Header Len,Bwd Header Len,Fwd Pkts/s,Bwd Pkts/s,Pkt Len Min,Pkt Len Max,Pkt Len Mean,Pkt Len Std,Pkt Len Var,FIN Flag Cnt,SYN Flag Cnt,RST Flag Cnt,PSH Flag Cnt,ACK Flag Cnt,URG Flag Cnt,CWE Flag Count,ECE Flag Cnt,Down/Up Ratio,Pkt Size Avg,Fwd Seg Size Avg,Bwd Seg Size Avg,Fwd Byts/b Avg,Fwd Pkts/b Avg,Fwd Blk Rate Avg,Bwd Byts/b Avg,Bwd Pkts/b Avg,Bwd Blk Rate Avg,Subflow Fwd Pkts,Subflow Fwd Byts,Subflow Bwd Pkts,Subflow Bwd Byts,Init Fwd Win Byts,Init Bwd Win Byts,Fwd Act Data Pkts,Fwd Seg Size Min,Active Mean,Active Std,Active Max,Active Min,Idle Mean,Idle Std,Idle Max,Idle Min,Label"
PATH = 'D:\\Learning\\APT\\Data\\Flow\\Tong_hop'
PATH_Total = 'D:\\Learning\\APT\\Data\\Flow\\All'

def getFileName(path):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    return onlyfiles
	

#read file by path return lines value
def readFile(path):
	fila = open(path, "r")
	lines = fila.readlines()
	return lines

#write file by path and values (type list)
def writeFile(path, values):
    #print(values)
    fil = open(path, "a")
    count = 0
    for value in values:
        count = count + 1
        fil.write(value + ",")
        if count == len(values):
            fil.write("\n")
    fil.close()

def writeAPTMal(path, pathTrain):
    fileNames = getFileName(path)
    for fileName in fileNames:
        print(fileName)
        lines = readFile(path + "\\" + fileName)
        for line in lines:
            line = line.split(",")
            #print(line)
            
            if "Mal-" in line[len(line) - 1]:
                line[len(line) - 1] = "1"
                writeFile(pathTrain + "\\MalWithTitle.csv", line)
            else:
                line[len(line) - 1] = "0"
                writeFile(pathTrain + "\\NoMalWithTitle.csv", line)
        #break

Title_list = TITLE.split(",")
writeFile(PATH_Total + "\\MalWithTitle.csv", Title_list)
writeFile(PATH_Total + "\\NoMalWithTitle.csv", Title_list)
writeAPTMal(PATH, PATH_Total)

