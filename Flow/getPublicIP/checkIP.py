import requests
import ipaddress as checkIP
import time

fil = open("D:\\Learning\\APT\\apt-dataset\\Flow\\getPublicIP\\ips.txt","r")

lines = fil.readlines()
count = 0
for line in lines:
	fil1 = open("D:\\Learning\\APT\\apt-dataset\\Flow\\getPublicIP\\check_ip_virustotal.csv","a")
	fil2 = open("D:\\Learning\\APT\\apt-dataset\\Flow\\getPublicIP\\apt_ip_virustotal.csv","a")
	#print(line.replace("\n",""))
	line = line.replace("\n","")
	url = 'https://www.virustotal.com/vtapi/v2/ip-address/report'
	
	params = {'apikey':'3fc61ec4f002b530f1cbbbbb7a4d3b7c31183a46066dff2c6d8730ac8fe33932','ip':line}
	
	response = requests.get(url, params=params)
	print(response)	
	response_json = response.json()
	
	if response_json['verbose_msg'] == "Missing IP address" or len(response_json['detected_urls']) == 0:
		print(line + ":" + "No-APT")
		fil1.write(line + ",0,\n")
		fil1.close()
	else:
		print(line + ":" + "APT")
		fil1.write(line + ",1,\n")
		fil1.close()
		fil2.write(line + ",1,\n")
		fil2.close()
	count = count + 1
	if count % 4 == 0:
		print("---------------Delay 60s-------------")
		time.sleep(60)
	#print(len(response_json['detected_urls']))