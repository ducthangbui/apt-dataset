import pandas as pd
from os import listdir
from os.path import isfile, join

def getFileName(path):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    return onlyfiles


PATH = 'D:\\Research\\APT\\apt-dataset\\Flow\\Tong_hop_virustotal_changelabel'
PATH_SAVE = 'D:\\Research\\APT\\apt-dataset\\Flow\\Tong_hop_virustotal_changelabel_del_flag'
fileNames = getFileName(PATH)
for fileName in fileNames:
	print(fileName)
	df = pd.read_csv(PATH + "\\" + fileName)
	dfn = df.drop(columns=['Fwd PSH Flags','Bwd PSH Flags','Fwd URG Flags','Bwd URG Flags','FIN Flag Cnt','SYN Flag Cnt','RST Flag Cnt','PSH Flag Cnt','ACK Flag Cnt','URG Flag Cnt','CWE Flag Count','ECE Flag Cnt'])
	dfn.to_csv(PATH_SAVE + "\\" + fileName, index=False)